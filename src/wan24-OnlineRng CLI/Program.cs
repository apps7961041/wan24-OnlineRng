﻿using wan24.CLI;
using wan24.Core;
using wan24.OnlineRng;

await Bootstrap.Async().DynamicContext();
ErrorHandling.ErrorHandler = (info) => Logging.WriteError(info.Exception.ToString());
return await CliApi.RunAsync(args, cancellationToken: CancellationToken.None, typeof(CliHelpApi), typeof(OnlineRngCliApi)).DynamicContext();
