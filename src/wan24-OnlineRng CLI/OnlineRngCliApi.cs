﻿using System.ComponentModel;
using wan24.CLI;
using wan24.Core;
using wan24.Crypto;
using wan24.Crypto.BC;

namespace wan24.OnlineRng
{
    /// <summary>
    /// Online RNG
    /// </summary>
    [CliApi("rng")]
    public sealed class OnlineRngCliApi
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OnlineRngCliApi() { }

        /// <summary>
        /// Create random data
        /// </summary>
        /// <param name="len">Length in bytes</param>
        /// <param name="cipher">Stream cipher algorithm name</param>
        public async Task RngAsync(
            [CliApi(keyLessOffset: 0)]
            [DisplayText("Length")]
            [Description("Random data length in bytes (if not given or <1, endless random data will be written to STDOUT)")]
            int? len = null,
            [CliApi]
            [DisplayText("Stream cipher")]
            [Description("Stream cipher algorithm name")]
            string? cipher = null
            )
        {
            RND.UseDevRandom = RND.HasDevRandom;
            using StreamCipherRng rng = new(
                cipher is not null
                    ? EncryptionHelper.GetAlgorithm(cipher)
                    : EncryptionChaCha20Algorithm.Instance,
                seedLength: null
                );
            RND.SeedConsumer = rng;
            using (RngOnlineSeedTimer onlineSeed = new())
                await onlineSeed.SeedAsync().DynamicContext();
            if (!len.HasValue || len.Value < 1)
            {
                // Inifinite stream
                using RentedArrayStructSimple<byte> buffer = new(Settings.BufferSize, clean: false)
                {
                    Clear = true
                };
                rng.FillBytes(buffer.Span);
                using Stream stdout = Console.OpenStandardOutput();
                for (; ; )
                {
                    await stdout.WriteAsync(buffer.Memory).DynamicContext();
                    rng.FillBytes(buffer.Span);
                }
            }
            else
            {
                // Requested amount of byte
                using RentedArrayStructSimple<byte> buffer = new(len.Value, clean: false)
                {
                    Clear = true
                };
                rng.FillBytes(buffer.Span);
                using Stream stdout = Console.OpenStandardOutput();
                await stdout.WriteAsync(buffer.Memory).DynamicContext();
            }
        }
    }
}
