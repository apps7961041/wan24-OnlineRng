﻿using wan24.Core;
using wan24.Crypto.BC;

namespace wan24.OnlineRng.Dto
{
    /// <summary>
    /// Statistics response DTO
    /// </summary>
    public sealed record class StatisticsDto
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="rdpPool">Random data provider pool (won't be disposed)</param>
        public StatisticsDto(BlockingObjectPool<RandomDataProvider> rdpPool)
        {
            UsedMemory = Environment.WorkingSet;
            RdpChilds = rdpPool.Initialized;
        }

        /// <summary>
        /// Used memory in byte
        /// </summary>
        public long UsedMemory { get; }

        /// <summary>
        /// Number of served random data provider childs
        /// </summary>
        public int RdpChilds { get; }
    }
}
