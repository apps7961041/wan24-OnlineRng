﻿namespace wan24.OnlineRng.Dto
{
    /// <summary>
    /// JSON response DTO
    /// </summary>
    public sealed record class JsonResponseDto
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public JsonResponseDto() { }

        /// <summary>
        /// <see cref="Seed"/> encoding
        /// </summary>
        public required SeedEncodings SeedEncoding { get; init; }

        /// <summary>
        /// Decoded <see cref="Seed"/> length in byte
        /// </summary>
        public required int Length { get; init; }

        /// <summary>
        /// Seed (encoded in the specified <see cref="SeedEncoding"/>)
        /// </summary>
        public required string Seed { get; init; }

        /// <summary>
        /// Seed format enumeration
        /// </summary>
        public enum SeedEncodings
        {
            /// <summary>
            /// base64 encoded
            /// </summary>
            Base64,
            /// <summary>
            /// Hexadecimal
            /// </summary>
            Hexadecimal
        }
    }
}
