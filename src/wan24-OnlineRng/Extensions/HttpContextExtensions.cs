﻿using wan24.Core;
using wan24.Crypto.BC;

namespace wan24.OnlineRng.Extensions
{
    /// <summary>
    /// <see cref="HttpContext"/> extensions
    /// </summary>
    public static class HttpContextExtensions
    {
        /// <summary>
        /// Get the prepared random data provider
        /// </summary>
        /// <param name="context">Context</param>
        /// <returns><see cref="RandomDataProvider"/> as rented object (if disposed, it'll be returned to the pool for re-use)</returns>
        public static RentedObject<RandomDataProvider> GetRandomDataProvider(this HttpContext context)
            => context.Items[nameof(RandomDataProvider)] as RentedObject<RandomDataProvider> ??
                throw new InvalidOperationException($"No {nameof(RandomDataProvider)} prepared for the current http context");
    }
}
