﻿using System.ComponentModel.DataAnnotations;
using wan24.Core;
using wan24.Crypto;
using wan24.Crypto.BC;
using wan24.ObjectValidation;

namespace wan24.OnlineRng.Config
{
    /// <summary>
    /// App settings
    /// </summary>
    public sealed record class AppSettings : ValidatableRecordBase
    {
        /// <summary>
        /// Section name
        /// </summary>
        public const string SECTION_NAME = "OnlineRng";

        /// <summary>
        /// Main random data provider instance
        /// </summary>
        private static RandomDataProvider? _RDP = null;
        /// <summary>
        /// Random data provider pool
        /// </summary>
        private static BlockingObjectPool<RandomDataProvider>? _RdpPool;

        /// <summary>
        /// Constructor
        /// </summary>
        static AppSettings()
        {
            Root = new ConfigurationBuilder().AddJsonFile(Path.Combine(Path.GetDirectoryName(typeof(AppSettings).Assembly.Location)!, "appsettings.json")).Build();
            Current = Root.GetSection(SECTION_NAME).Get<AppSettings>()?.ValidateObject(out _) ??
                throw new InvalidDataException($"Failed to load {nameof(AppSettings)} from appsettings.json section {SECTION_NAME}");
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public AppSettings() : base() { }

        /// <summary>
        /// App settings root
        /// </summary>
        public static IConfigurationRoot Root { get; }

        /// <summary>
        /// Currrent app settings
        /// </summary>
        public static AppSettings Current { get; }

        /// <summary>
        /// Main random data provider instance
        /// </summary>
        public static RandomDataProvider RDP => _RDP ?? throw new InvalidOperationException();

        /// <summary>
        /// Random data provider pool
        /// </summary>
        public static BlockingObjectPool<RandomDataProvider> RdpPool => _RdpPool ?? throw new InvalidOperationException();

        /// <summary>
        /// Logfile
        /// </summary>
        [MinLength(1), MaxLength(1024)]
        public string? LogFile { get; init; }

        /// <summary>
        /// Log level
        /// </summary>
        public required LogLevel LogLevel { get; init; }

        /// <summary>
        /// Number of RNGs to serve in parallel (or <c>0</c> to use the number of processors; will be used to limit the <c>/dev/random</c> stream pool, too)
        /// </summary>
        [Range(0, int.MaxValue)]
        public int RngCount { get; init; }

        /// <summary>
        /// The external base URI of this web app (without trailing slash)
        /// </summary>
        public string? ExternalUri { get; set; }

        /// <summary>
        /// Stream cipher algorithm name (will be used to wrap the RNG)
        /// </summary>
        [MinLength(1), MaxLength(byte.MaxValue)]
        public string? StreamCipherAlgorithmName { get; set; }

        /// <summary>
        /// Default random data source seed request interval in ms (default is 1m)
        /// </summary>
        [Range(1000, int.MaxValue)]
        public int DefaultRandomSeedInterval { get; set; }

        /// <summary>
        /// Bearer token for authenticated service calls
        /// </summary>
        public string? BearerToken { get; set; }

        /// <summary>
        /// Get the stream cipher algorithm
        /// </summary>
        /// <returns>Stream cipher algorithm</returns>
        public EncryptionAlgorithmBase GetStreamCipherAlgorithm() => EncryptionHelper.GetAlgorithm(StreamCipherAlgorithmName ?? EncryptionChaCha20Algorithm.ALGORITHM_NAME);

        /// <summary>
        /// Apply the app configuration
        /// </summary>
        /// <param name="isDevelopment">Is the development environment?</param>
        public static async Task ApplyAsync(bool isDevelopment)
        {
            // Logging
            ErrorHandling.ErrorHandler = (info) => info.Exception.ToString().WriteError();
            Logging.Logger = Current.LogFile is not null
                ? await FileLogger.CreateAsync(
                    Current.LogFile,
                    isDevelopment
                        ? LogLevel.Trace
                        : Current.LogLevel,
                    new ConsoleLogger(
                        isDevelopment
                            ? LogLevel.Trace
                            : Current.LogLevel
                        )
                    ).DynamicContext()
                : new ConsoleLogger(
                    isDevelopment
                        ? LogLevel.Trace
                        : Current.LogLevel
                    );
            // Main random data provider
            StreamCipherRng csRng = new(Current.GetStreamCipherAlgorithm(), bufferSize: QrngSeeder.MAX_SEED_LENGTH);
            csRng.OnDisposing += (s, e) => RND.Generator = null;
            _RDP = await RandomDataProvider.CreateAsync(
                    capacity: QrngSeeder.MAX_SEED_LENGTH,
                    workerBufferSize: QrngSeeder.MAX_SEED_LENGTH,
                    rng: csRng
                );
            _RDP.Name = "Main random data provider (seed consumer)";
            _RDP.OnDisposing += (s, e) => RND.SeedConsumer = null;
            // Random data provider pool
            _RdpPool = new BlockingObjectPool<RandomDataProvider>(
                    capacity: Current.RngCount > 0
                        ? Current.RngCount
                        : Environment.ProcessorCount,
                    async () =>
                    {
                        "Creating a new random data provider instance".WriteDebug();
                        RandomDataProvider res = await RDP.CreateForkAsync(
                            rng: new StreamCipherRng(Current.GetStreamCipherAlgorithm(), bufferSize: QrngSeeder.MAX_SEED_LENGTH)
                            ).DynamicContext();
                        try
                        {
                            res.Name = "Random data provider attached child";
                            await res.StartAsync().DynamicContext();
                            return res;
                        }
                        catch
                        {
                            await res.DisposeAsync().DynamicContext();
                            throw;
                        }
                    }
                )
            {
                Name = "Random data provider fork pool"
            };
            // wan24-Crypto configuration
            CryptoEnvironment.Configure(new()
            {
                UseDevRandom = RND.HasDevRandom,
                DevRandomPool = RND.HasDevRandom
                    ? new DevRandomStreamPool(
                        capacity: Current.RngCount > 0
                            ? Current.RngCount + 1
                            : Environment.ProcessorCount + 1
                        )
                    : null,
                RandomGenerator = csRng,
                SeedConsumer = _RDP
            });
        }
    }
}
