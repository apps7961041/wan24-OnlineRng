﻿using System.Security.Cryptography;
using wan24.Core;
using wan24.Crypto;
using wan24.OnlineRng.Config;

namespace wan24.OnlineRng.Services
{
    /// <summary>
    /// Seed <see cref="RND"/> with 256 byte entropy from the default RNG (<c>/dev/random</c> or <see cref="RandomNumberGenerator"/>)
    /// </summary>
    public sealed class DefaultSeeder : TimedHostedServiceBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public DefaultSeeder() : base(AppSettings.Current.DefaultRandomSeedInterval) => Name = "Default seed timer";

        /// <inheritdoc/>
        protected override async Task TimedWorkerAsync()
        {
            using RentedArrayStructSimple<byte> buffer = new(len: 256, clean: false)
            {
                Clear = true
            };
            await RND.DefaultRngAsync(buffer.Memory).DynamicContext();
            await RND.AddSeedAsync(buffer.Memory, CancelToken).DynamicContext();
        }
    }
}
