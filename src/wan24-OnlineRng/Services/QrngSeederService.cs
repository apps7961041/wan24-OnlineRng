﻿using wan24.Core;

namespace wan24.OnlineRng.Services
{
    /// <summary>
    /// QRNG seeder service
    /// </summary>
    public sealed class QrngSeederService : HostedServiceBase
    {
        /// <summary>
        /// Hosted seeder services
        /// </summary>
        private readonly IHostedService[] SeederServices;

        /// <summary>
        /// Constructor
        /// </summary>
        public QrngSeederService() : base()
        {
            Name = "QRNG seeder service";
            SeederServices = new IHostedService[QrngSeeder.Services.Count];
            try
            {
                for (int i = 0, len = QrngSeeder.Services.Count; i < len; i++)
                    SeederServices[i] = (IHostedService?)QrngSeeder.Services[i].ConstructAuto() ?? 
                        throw new InvalidDataException($"Invalid seeder type {QrngSeeder.Services[i]}");
            }
            catch
            {
                Dispose();
                throw;
            }
        }

        /// <inheritdoc/>
        protected override async Task WorkerAsync() => await CancelToken.WaitHandle.WaitAsync().DynamicContext();

        /// <inheritdoc/>
        protected override async Task StartingAsync(CancellationToken cancellationToken)
        {
            bool started = false;
            foreach (IHostedService seederService in SeederServices)
                try
                {
                    await seederService.StartAsync(cancellationToken).DynamicContext();
                    started = true;
                }
                catch (Exception ex)
                {
                    Logging.WriteError($"Failed to start seeder service {seederService.GetType()}: {ex}");
                }
            if (!started) throw new InvalidOperationException("Failed to start any QRNG seeder service");
        }

        /// <inheritdoc/>
        protected override async Task StoppingAsync(CancellationToken cancellationToken)
        {
            foreach (IHostedService seederService in SeederServices)
                try
                {
                    await seederService.StopAsync(cancellationToken).DynamicContext();
                }
                catch(Exception ex)
                {
                    Logging.WriteError($"Failed to stop seeder service {seederService.GetType()}: {ex}");
                }
        }

        /// <inheritdoc/>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            SeederServices.TryDisposeAll();
        }

        /// <inheritdoc/>
        protected override async Task DisposeCore()
        {
            await base.DisposeCore().DynamicContext();
            await SeederServices.TryDisposeAllAsync().DynamicContext();
        }
    }
}
