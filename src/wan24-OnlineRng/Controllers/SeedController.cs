﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Buffers;
using System.Buffers.Text;
using System.ComponentModel.DataAnnotations;
using wan24.Core;
using wan24.Crypto.BC;
using wan24.OnlineRng.Dto;
using wan24.OnlineRng.Extensions;

namespace wan24.OnlineRng.Controllers
{
    /// <summary>
    /// Seed API controller
    /// </summary>
    [ApiController, Route(API_ROUTE), EnableCors]
    public sealed class SeedController : ControllerBase
    {
        /// <summary>
        /// Binary response MIME type
        /// </summary>
        private const string BIN_MIME_TYPE = "application/octet-stream";
        /// <summary>
        /// Text response MIME type
        /// </summary>
        private const string TXT_MIME_TYPE = "text/plain";
        /// <summary>
        /// JSON response MIME type
        /// </summary>
        private const string JSN_MIME_TYPE = "application/json";
        /// <summary>
        /// Content attachment disposition
        /// </summary>
        private const string ATTACHMENT_DISPOSITION = "attachment; filename=rnd.bin";
        /// <summary>
        /// API route
        /// </summary>
        public const string API_ROUTE = "seed";
        /// <summary>
        /// Default responded seed length in byte
        /// </summary>
        public const int DEFAULT_SEED_LENGTH = 256;

        /// <summary>
        /// Constructor
        /// </summary>
        public SeedController() : base() { }

        /// <summary>
        /// Binary output (file download)
        /// </summary>
        /// <param name="len">Requested random data length in byte</param>
        [HttpGet, Route("bin")]
        [ProducesResponseType(typeof(byte[]), StatusCodes.Status200OK, BIN_MIME_TYPE)]
        public async Task BinaryAsync([Range(QrngSeeder.MIN_SEED_LENGTH, QrngSeeder.MAX_SEED_LENGTH), FromQuery] int len = DEFAULT_SEED_LENGTH)
        {
            RentedArray<byte> buffer = await GetRandomDataAsync(len).DynamicContext();
            try
            {
                HttpContext context = HttpContext;
                HttpResponse response = context.Response;
                IHeaderDictionary header = response.Headers;
                header.ContentType = BIN_MIME_TYPE;
                header.ContentDisposition = ATTACHMENT_DISPOSITION;
                header.ContentLength = len;
                await response.Body.WriteAsync(buffer.Memory, context.RequestAborted).DynamicContext();
            }
            catch (OperationCanceledException)
            {
            }
            finally
            {
                buffer.Dispose();
            }
        }

        /// <summary>
        /// Hexadecimal encoded output
        /// </summary>
        /// <param name="len">Requested random data length in byte</param>
        /// <returns>Hexadecimal encoded string response</returns>
        [HttpGet, Route("hex")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK, TXT_MIME_TYPE)]
        public async Task<ContentResult> HexAsync([Range(QrngSeeder.MIN_SEED_LENGTH, QrngSeeder.MAX_SEED_LENGTH), FromQuery] int len = DEFAULT_SEED_LENGTH)
        {
            using RentedArray<byte> buffer = await GetRandomDataAsync(len).DynamicContext();
            return Content(Convert.ToHexString(buffer.Span));
        }

        /// <summary>
        /// base64 encoded output
        /// </summary>
        /// <param name="len">Requested random data length in byte</param>
        /// <returns>Base64 encoded string response or an error message</returns>
        [HttpGet, Route("b64")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK, TXT_MIME_TYPE)]
        [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError, TXT_MIME_TYPE)]
        public async Task<ContentResult> Base64Async([Range(QrngSeeder.MIN_SEED_LENGTH, QrngSeeder.MAX_SEED_LENGTH), FromQuery] int len = DEFAULT_SEED_LENGTH)
        {
            using RentedArray<byte> buffer = await GetRandomDataAsync(Base64.GetMaxEncodedToUtf8Length(len), len).DynamicContext();
            OperationStatus status = Base64.EncodeToUtf8InPlace(buffer.Span, len, out int encodedLen);
            if (status == OperationStatus.Done) return Content(buffer.Span[..encodedLen].ToUtf8String());
            Logging.WriteError($"Invalid base64 encoding operation status {status} when responding {len} byte random data as plain text");
            HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
            return Content("Result base64 encoding failed");
        }

        /// <summary>
        /// JSON object output
        /// </summary>
        /// <param name="len">Requested random data length in byte</param>
        /// <param name="seedEncoding">Seed encoding</param>
        /// <returns>JSON response object</returns>
        [HttpGet, Route("jsn")]
        [ProducesResponseType(typeof(JsonResponseDto), StatusCodes.Status200OK, JSN_MIME_TYPE)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError, JSN_MIME_TYPE)]
        public async Task<IActionResult> JsonAsync(
            [Range(QrngSeeder.MIN_SEED_LENGTH, QrngSeeder.MAX_SEED_LENGTH), FromQuery] int len = DEFAULT_SEED_LENGTH,
            [FromQuery] JsonResponseDto.SeedEncodings seedEncoding = JsonResponseDto.SeedEncodings.Base64
            )
        {
            string seed;
            switch (seedEncoding)
            {
                case JsonResponseDto.SeedEncodings.Base64:
                    using (RentedArray<byte> buffer = await GetRandomDataAsync(Base64.GetMaxEncodedToUtf8Length(len), len).DynamicContext())
                    {
                        OperationStatus status = Base64.EncodeToUtf8InPlace(buffer.Span, len, out int encodedLen);
                        if (status != OperationStatus.Done)
                        {
                            Logging.WriteError($"Invalid base64 encoding operation status {status} when responding {len} byte random data as JSON object");
                            return Problem("Result base64 encoding failed", statusCode: StatusCodes.Status500InternalServerError);
                        }
                        seed = buffer.Span[..encodedLen].ToUtf8String();
                    }
                    break;
                case JsonResponseDto.SeedEncodings.Hexadecimal:
                    using (RentedArray<byte> buffer = await GetRandomDataAsync(len).DynamicContext())
                        seed = Convert.ToHexString(buffer.Span);
                    break;
                default:
                    return BadRequest();
            }
            return Ok(new JsonResponseDto()
            {
                SeedEncoding = seedEncoding,
                Length = len,
                Seed = seed
            });
        }

        /// <summary>
        /// Get a random byte sequence
        /// </summary>
        /// <param name="len">Total buffer length in byte</param>
        /// <param name="rndLen">Required random data length in byte (or <c>0</c> to use the <c>len</c> value)</param>
        /// <returns>Rented buffer which contains the requested random data</returns>
        private async Task<RentedArray<byte>> GetRandomDataAsync(int len, int rndLen = 0)
        {
            RentedArray<byte> res = new(len, clean: false)
            {
                Clear = true
            };
            try
            {
                using RentedObject<RandomDataProvider> rentedRdp = HttpContext.GetRandomDataProvider();
                await rentedRdp.Object.FillBytesAsync(len == rndLen ? res.Memory : res.Memory[..(rndLen == 0 ? len : rndLen)]).DynamicContext();
                return res;
            }
            catch
            {
                res.Dispose();
                throw;
            }
        }
    }
}
