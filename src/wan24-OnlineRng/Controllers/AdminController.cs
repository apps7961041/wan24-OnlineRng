﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using wan24.OnlineRng.Authorization;
using wan24.OnlineRng.Dto;

namespace wan24.OnlineRng.Controllers
{
    /// <summary>
    /// Administration API controller
    /// </summary>
    [ApiController, Route(API_ROUTE), LocalhostOnly, EnableCors]
    public sealed class AdminController : ControllerBase
    {
        /// <summary>
        /// API route
        /// </summary>
        public const string API_ROUTE = "admin";

        /// <summary>
        /// Constructor
        /// </summary>
        public AdminController() : base() { }

        /// <summary>
        /// Perform a shutdown
        /// </summary>
        /// <param name="lifetime">Lifetime</param>
        [HttpGet, Route("shutdown")]
        public void ShutdownAsync(IHostApplicationLifetime lifetime) => lifetime.StopApplication();

        /// <summary>
        /// Get statistics
        /// </summary>
        /// <param name="statistics">Statistics</param>
        /// <returns>Statistics</returns>
        [HttpGet, Route("stats")]
        [ProducesResponseType(typeof(StatisticsDto), StatusCodes.Status200OK)]
        public StatisticsDto Statistics(StatisticsDto statistics) => statistics;
    }
}
