﻿using wan24.Core;
using wan24.Crypto.BC;
using wan24.OnlineRng.Config;
using wan24.OnlineRng.Controllers;

namespace wan24.OnlineRng.Middleware
{
    /// <summary>
    /// Limit the number of requests using the <see cref="RandomDataProvider"/> object pool limit, prepare a <see cref="RandomDataProvider"/> for providing seed to the peer and 
    /// redirect to SwaggerUI per default
    /// </summary>
    /// <remarks>
    /// Constructor
    /// </remarks>
    /// <param name="next">Next request handler</param>
    /// <param name="rdpPool">Random data provider pool (won't be disposed)</param>
    public sealed class RngMiddleware(RequestDelegate next, BlockingObjectPool<RandomDataProvider> rdpPool)
    {
        /// <summary>
        /// Seed API route prefix
        /// </summary>
        private const string SEED_API_PREFIX = $"/{SeedController.API_ROUTE}/";
        /// <summary>
        /// http GET method
        /// </summary>
        private const string HTTP_GET_METHOD = "GET";
        /// <summary>
        /// Connection close
        /// </summary>
        private const string CLOSE_CONNECTION = "Close";

        /// <summary>
        /// SwaggerUI URI
        /// </summary>
        private static readonly string SwaggerUri = $"{AppSettings.Current.ExternalUri}/swagger";

        /// <summary>
        /// Next request handler
        /// </summary>
        private readonly RequestDelegate Next = next;
        /// <summary>
        /// Random data provider pool
        /// </summary>
        private readonly BlockingObjectPool<RandomDataProvider> RdpPool = rdpPool;

        /// <summary>
        /// Invoke
        /// </summary>
        /// <param name="context">Context</param>
        public async Task InvokeAsync(HttpContext context)
        {
            HttpRequest request = context.Request;
            // Handle the request path for GET requests
            if (request.Method == HTTP_GET_METHOD)
                if (!request.Path.HasValue || (request.Path.Value.Length == 1 && request.Path.Value[0] == '/'))
                {
                    // Redirect to SwaggerUI per default
                    HttpResponse response = context.Response;
                    response.Clear();
                    response.StatusCode = StatusCodes.Status308PermanentRedirect;
                    response.Headers.Location = SwaggerUri;
                    return;
                }
                else if (request.Path.HasValue && request.Path.Value.StartsWith(SEED_API_PREFIX))
                {
                    // Prepare seed generation
                    HttpResponse response = context.Response;
                    TryAsyncResult<RandomDataProvider> rdp = await RdpPool.TryRentAsync().DynamicContext();
                    if (!rdp)
                    {
                        // The max. number of parallel processing API requests was limited by the availability of a pooled RandomDataProvider instance
                        response.Clear();
                        response.StatusCode = StatusCodes.Status429TooManyRequests;
                        response.Headers.Connection = CLOSE_CONNECTION;
                        return;
                    }
                    RentedObject<RandomDataProvider> rentedRdp = new(RdpPool, rdp);
                    response.RegisterForDisposeAsync(rentedRdp);
                    context.Items[nameof(RandomDataProvider)] = rentedRdp;
                }
            // Ensure to close the connection when the seed API was requested
            if (context.Items.ContainsKey(nameof(RandomDataProvider))) context.Response.Headers.Connection = CLOSE_CONNECTION;
            // Execute the next middleware(s)
            await Next(context).DynamicContext();
        }
    }
}
