using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.HttpOverrides;
using NSwag;
using System.Text.Json.Serialization;
using wan24.Core;
using wan24.OnlineRng;
using wan24.OnlineRng.Authentication;
using wan24.OnlineRng.Authorization;
using wan24.OnlineRng.Config;
using wan24.OnlineRng.Controllers;
using wan24.OnlineRng.Dto;
using wan24.OnlineRng.Middleware;
using wan24.OnlineRng.Services;

//TODO .NET 8: Request timeouts https://learn.microsoft.com/en-us/aspnet/core/performance/timeouts?view=aspnetcore-8.0
//TODO With .NET 9 maybe use implemented OpenAPI

// Build the app
await Bootstrap.Async(typeof(SeedController).Assembly).DynamicContext();
//HuBootstrap.Boot();//FIXME Shouldn't be required!
OthBootstrap.Boot();//FIXME Shouldn't be required!
WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
await AppSettings.ApplyAsync(builder.Environment.IsDevelopment()).DynamicContext();
builder.WebHost.ConfigureKestrel(options => options.AddServerHeader = false);
builder.Logging.SetMinimumLevel(
        builder.Environment.IsDevelopment()
            ? LogLevel.Trace
            : AppSettings.Current.LogLevel
    )
    .AddProvider(new LoggerProvider(Logging.Logger!));
builder.Services.AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.WriteIndented = true;
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    });
builder.Services.AddAuthorization(options => options.AddPolicy(
            nameof(LocalhostRequirement),
            new AuthorizationPolicy(
                [new LocalhostRequirement()],
                [AuthenticationHandler.SCHEME]
            ))
        )
    .AddAuthentication(options => options.DefaultAuthenticateScheme = AuthenticationHandler.SCHEME)
    .AddScheme<AuthenticationSchemeOptions, AuthenticationHandler>(AuthenticationHandler.SCHEME, options => { });
builder.Services.AddSingleton<IAuthorizationHandler, LocalhostAuthorizationHandler>()
    .AddSingleton(serviceProvider => AppSettings.RdpPool)
    .AddHostedService(serviceProvider => AppSettings.RDP)
    .AddHostedService<DefaultSeeder>()
    .AddHostedService<QrngSeederService>()
    .AddTransient<StatisticsDto>()
    .AddCors(options => options.AddDefaultPolicy(policy => policy.AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod()
        ))
    .AddSwaggerDocument(config =>
    {
        config.AddSecurity("bearer", [], new OpenApiSecurityScheme
        {
            Type = OpenApiSecuritySchemeType.ApiKey,
            Name = "Authorization",
            Description = "Bearer token",
            In = OpenApiSecurityApiKeyLocation.Header,
            Scheme = AuthenticationHandler.SCHEME,
            BearerFormat = "Token"
        });
        config.PostProcess = document => document.Info = new()
        {
            Version = "v1",
            Title = "Online RNG",
            Description = "RNG online API, hosted private for free usage, but without any warranty.",
            Contact = new()
            {
                Name = "nd",
                Email = "nd@wan24.de",
                Url = "https://gitlab.com/apps7961041/wan24-OnlineRng"
            },
            License = new()
            {
                Name = "MIT",
                Url = "https://gitlab.com/apps7961041/wan24-OnlineRng/-/blob/main/LICENSE?ref_type=heads"
            }
        };
    })
    .AddResponseCompression();
WebApplication app = builder.Build();

// Run the app
Logging.WriteInfo("Starting Online RNG");
await using (app.DynamicContext())
{
    app.UseResponseCompression()
        .UseExceptionHandler(app => app.Run(context =>
        {
            //TODO .NET 8: Use an IExceptionHandler
            Logging.WriteError(
                context.Features.Get<IExceptionHandlerFeature>() is IExceptionHandlerFeature ehf
                    ? $"Peer \"{context.Connection.RemoteIpAddress}\" endpoint \"{ehf.Endpoint}\" request path \"{ehf.Path}\" handling caused an exception: {ehf.Error}"
                    : $"App pipeline catched an exception during peer \"{context.Connection.RemoteIpAddress}\" endpoint \"{context.GetEndpoint()}\" request path \"{context.Request.Path}\" handling, but no {nameof(IExceptionHandlerFeature)} is available :("
                );
            if (!context.Response.HasStarted)
            {
                context.Response.Clear();
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            }
            return Task.CompletedTask;
        }))
        .UseForwardedHeaders(new()
        {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
        })
        .UseCors()
        .UseOpenApi(options => options.PostProcess = (document, request) =>
        {
            document.Servers.Clear();
            document.Servers.Add(new()
            {
                Description = "Online RNG API",
                Url = app.Environment.IsDevelopment()
                    ? AppSettings.Root.GetValue<string>("Urls")
                    : AppSettings.Current.ExternalUri ?? AppSettings.Root.GetValue<string>("Urls")
            });
        })
        .UseSwaggerUi()
        .UseMiddleware<RngMiddleware>()
        .UseAuthorization();
    app.MapControllers();
    await app.RunAsync().DynamicContext();
}
Logging.WriteInfo("Stopped Online RNG");
