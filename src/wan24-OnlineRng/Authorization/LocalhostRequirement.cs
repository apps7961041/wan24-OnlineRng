﻿using Microsoft.AspNetCore.Authorization;
using wan24.Core;

namespace wan24.OnlineRng.Authorization
{
    /// <summary>
    /// Requires a http request from the localhost
    /// </summary>
    public sealed class LocalhostRequirement : IAuthorizationRequirement
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LocalhostRequirement() { }

        /// <summary>
        /// Determine if authorized
        /// </summary>
        /// <param name="context">http context</param>
        /// <returns>If authorized</returns>
        public bool MetsRequirement(HttpContext context) => context.Connection.RemoteIpAddress?.IsLoopBack() ?? false;
    }
}
