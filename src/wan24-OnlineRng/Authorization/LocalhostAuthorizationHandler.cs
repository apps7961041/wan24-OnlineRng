﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;

namespace wan24.OnlineRng.Authorization
{
    /// <summary>
    /// Authorization handler for a <see cref="LocalhostRequirement"/>
    /// </summary>
    public sealed class LocalhostAuthorizationHandler : AuthorizationHandler<LocalhostRequirement>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LocalhostAuthorizationHandler() : base() { }

        /// <inheritdoc/>
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, LocalhostRequirement requirement)
        {
            if ((context.Resource as HttpContext ?? (context.Resource as AuthorizationFilterContext)?.HttpContext) is not HttpContext httpContext)
            {
                context.Fail(new AuthorizationFailureReason(this, $"Missing {nameof(HttpContext)} (got {context.Resource?.GetType().ToString() ?? "no resource"} instead)"));
            }
            else if (requirement.MetsRequirement(httpContext))
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail(new AuthorizationFailureReason(this, $"Requirement \"{requirement}\" wasn't met"));
            }
            return Task.CompletedTask;
        }
    }
}
