﻿using Microsoft.AspNetCore.Authorization;

namespace wan24.OnlineRng.Authorization
{
    /// <summary>
    /// Attribute for authorizing request from the localhost only
    /// </summary>
    public sealed class LocalhostOnlyAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LocalhostOnlyAttribute() : base(nameof(LocalhostRequirement)) { }
    }
}
