﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Text.Encodings.Web;
using wan24.OnlineRng.Config;

namespace wan24.OnlineRng.Authentication
{
    /// <summary>
    /// Authentication handler
    /// </summary>
    /// <remarks>
    /// Constructor
    /// </remarks>
    /// <param name="optionsMonitor">Options monitor</param>
    /// <param name="loggerFactory">Logger factory</param>
    /// <param name="urlEncoder">URL encoder</param>
    internal sealed class AuthenticationHandler(
        IOptionsMonitor<AuthenticationSchemeOptions> optionsMonitor,
        ILoggerFactory loggerFactory,
        UrlEncoder urlEncoder
            ) : AuthenticationHandler<AuthenticationSchemeOptions>(optionsMonitor, loggerFactory, urlEncoder)
    {
        /// <summary>
        /// Authentication scheme
        /// </summary>
        public const string SCHEME = "Bearer";

        /// <inheritdoc/>
        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (Context.Request.Headers.Authorization.Count == 0) return Task.FromResult(AuthenticateResult.NoResult());
            if (
                    AppSettings.Current.BearerToken is not null &&
                    Context.Request.Headers.Authorization.Count == 1 &&
                    Context.Request.Headers.Authorization[0] is string auth &&
                    auth.Length > SCHEME.Length + 1 &&
                    auth.StartsWith(SCHEME) &&
                    auth[(SCHEME.Length + 1)..] == AppSettings.Current.BearerToken
                    )
                Task.FromResult(AuthenticateResult.Success(new(new(), SCHEME)));
            return Task.FromResult(AuthenticateResult.Fail("Invalid authentication"));
        }
    }
}
