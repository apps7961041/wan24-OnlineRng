﻿using System.Net.Http.Json;
using wan24.Core;
using wan24.Crypto;
using wan24.OnlineRng.Dto;

namespace wan24.OnlineRng.Services
{
    /// <summary>
    /// QRNG seed timer
    /// </summary>
    public sealed class OthQrngSeeder : RngOnlineSeedTimer
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public OthQrngSeeder() : base(double.Parse(Properties.Resources.SEED_INTERVAL), uri: Properties.Resources.QRNG_URI)
        {
            Name = "OTH QRNG seeder";
            Http.Timeout = TimeSpan.FromSeconds(3);
        }

        /// <inheritdoc/>
        protected override async Task<int> RequestSeedAsync(CancellationToken cancellationToken)
        {
            try
            {
                QrnDto? qrnd = await Http.GetFromJsonAsync<QrnDto>(URI, cancellationToken).DynamicContext();
                if (qrnd is null)
                {
                    Logging.WriteError($"{GetType()} failed to receive a {typeof(QrnDto)} from \"{URI}\"");
                    return 0;
                }
                if (qrnd.QrnBytes.Length == 0)
                {
                    Logging.WriteWarning($"{GetType()} got no seed from \"{URI}\"");
                    return 0;
                }
                using SecureByteArrayStructSimple qrnBytes = new(qrnd.QrnBytes);
                Logging.WriteDebug($"{GetType()} got {qrnBytes.Length} byte seed from \"{URI}\"");
                if (qrnBytes.Length > Seed.Length)
                {
                    Logging.WriteWarning($"{GetType()} got more seed than expected from \"{URI}\" (expected {Seed.Length} byte, received {qrnBytes.Length} byte)");
                    qrnBytes.Span[..Seed.Length].CopyTo(Seed);
                    return Seed.Length;
                }
                qrnBytes.Span.CopyTo(Seed);
                if (qrnBytes.Length != Seed.Length)
                    Logging.WriteWarning($"{GetType()} expected {Seed.Length} byte seed, but got only {qrnBytes.Length} byte instead from \"{URI}\"");
                return qrnBytes.Length;
            }
            catch(Exception ex)
            {
                Logging.WriteError($"{GetType()} failed to receive a {typeof(QrnDto)} from \"{URI}\": {ex}");
                return 0;
            }
        }
    }
}
