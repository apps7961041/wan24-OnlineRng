﻿using System.ComponentModel.DataAnnotations;
using wan24.ObjectValidation;

namespace wan24.OnlineRng.Dto
{
    /// <summary>
    /// QRN response DTO
    /// </summary>
    public sealed record class QrnDto : ValidatableRecordBase
    {
        /// <summary>
        /// Decoded <see cref="Qrn"/>
        /// </summary>
        private byte[]? _QrnBytes = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public QrnDto() : base() { }

        /// <summary>
        /// Decoded <see cref="Qrn"/> length in byte
        /// </summary>
        [Range(QrngSeeder.MIN_SEED_LENGTH, QrngSeeder.MAX_SEED_LENGTH)]
        public required int Length { get; init; }

        /// <summary>
        /// Quantum random numbers (as hex encoded string)
        /// </summary>
        [Required, MinLength(QrngSeeder.MIN_SEED_LENGTH << 1), MaxLength(QrngSeeder.MAX_SEED_LENGTH << 1)]
        public required string Qrn { get; init; }

        /// <summary>
        /// Decoded <see cref="Qrn"/>
        /// </summary>
        [NoValidation]
        public byte[] QrnBytes => _QrnBytes ??= Convert.FromHexString(Qrn);

        /// <inheritdoc/>
        protected override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            foreach (ValidationResult result in base.Validate(validationContext)) yield return result;
            if (Length != QrnBytes.Length) yield return new("Reported QRN length mismatch", new string[] { nameof(Length) });
        }
    }
}
