﻿using wan24.Core;
using wan24.OnlineRng.Services;

[assembly: Bootstrapper(typeof(wan24.OnlineRng.OthBootstrap), nameof(wan24.OnlineRng.OthBootstrap.Boot))]

namespace wan24.OnlineRng
{
    /// <summary>
    /// Bootstrapper
    /// </summary>
    public static class OthBootstrap
    {
        /// <summary>
        /// Boot
        /// </summary>
        public static void Boot() => QrngSeeder.Services.Add(typeof(OthQrngSeeder));
    }
}
