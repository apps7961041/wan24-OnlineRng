﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using wan24.Core;
using wan24.Crypto;

// For Windows client development: https://stackoverflow.com/a/41175223/10797322 -> https://www.microsoft.com/en-us/download/details.aspx?id=30688

// Parse arguments
CliArguments param = new(args);
double interval = param.ValueCount("interval") == 1
    ? int.Parse(param.Single("interval"))
    : TimeSpan.FromHours(8).TotalMilliseconds;
string uri = param.ValueCount("uri") == 1
    ? param.Single("uri")
    : RngOnlineSeedTimer.DEFAULT_URI;
int len = param.ValueCount("seed") == 1
    ? int.Parse(param.Single("seed"))
    : RngOnlineSeedTimer.DEFAULT_SEED_LENGTH;

// Initialize
await wan24.Core.Bootstrap.Async(typeof(Program).Assembly).DynamicContext();
ErrorHandling.ErrorHandler = (info) => Logging.WriteError(info.Exception.ToString());
Logging.Logger = new ConsoleLogger();
if (!RND.HasDevRandom) throw new InvalidOperationException($"{RND.RANDOM} isn't available");
RND.UseDevRandom = true;

// Run the seed service
HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);
builder.Logging.SetMinimumLevel(
        builder.Environment.IsDevelopment()
            ? LogLevel.Trace
            : LogLevel.Information
    )
    .AddProvider(new LoggerProvider(Logging.Logger));
builder.Services.AddHostedService(serviceProvider => new RngOnlineSeedTimer(interval, uri: uri, length: len));
using IHost host = builder.Build();
Logging.WriteInfo("Starting online RNG seed timer");
await host.RunAsync().DynamicContext();
Logging.WriteInfo("Stopped online RNG seed timer");
