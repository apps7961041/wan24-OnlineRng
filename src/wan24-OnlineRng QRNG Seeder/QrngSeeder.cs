﻿namespace wan24.OnlineRng
{
    /// <summary>
    /// QRNG seeder
    /// </summary>
    public static class QrngSeeder
    {
        /// <summary>
        /// Min. seed length in byte
        /// </summary>
        public const int MIN_SEED_LENGTH = 1;
        /// <summary>
        /// Max. seed length in byte
        /// </summary>
        public const int MAX_SEED_LENGTH = 1024;

        /// <summary>
        /// Services
        /// </summary>
        public static List<Type> Services { get; } = [];
    }
}
