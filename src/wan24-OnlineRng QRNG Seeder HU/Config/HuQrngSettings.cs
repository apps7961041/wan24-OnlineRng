﻿using System.ComponentModel.DataAnnotations;
using wan24.Core;

namespace wan24.OnlineRng.Config
{
    /// <summary>
    /// HU QRNG settings
    /// </summary>
    public sealed record class HuQrngSettings
    {
        /// <summary>
        /// Constructor
        /// </summary>
        static HuQrngSettings()
        {
            Current = JsonHelper.Decode<HuQrngSettings>(File.ReadAllText(Path.Combine(Path.GetDirectoryName(typeof(HuQrngSettings).Assembly.Location)!, "huseeder.json")))
                ?? throw new InvalidDataException("Failed to load huseeder.json");
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public HuQrngSettings() { }

        /// <summary>
        /// Current
        /// </summary>
        public static HuQrngSettings Current { get; }

        /// <summary>
        /// User
        /// </summary>
        [Required]
        public required string User { get; init; }

        /// <summary>
        /// Password
        /// </summary>
        [Required]
        public required string Password { get; init; }

        /// <summary>
        /// Use SSL?
        /// </summary>
        public bool SSL { get; init; }

        /// <summary>
        /// Seed interval in ms
        /// </summary>
        [Range(1, int.MaxValue)]
        public double SeedInterval { get; init; }
    }
}
