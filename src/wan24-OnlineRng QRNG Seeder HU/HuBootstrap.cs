﻿using wan24.Core;
using wan24.OnlineRng.Services;

[assembly: Bootstrapper(typeof(wan24.OnlineRng.HuBootstrap), nameof(wan24.OnlineRng.HuBootstrap.Boot))]

namespace wan24.OnlineRng
{
    /// <summary>
    /// Bootstrapper
    /// </summary>
    public static class HuBootstrap
    {
        /// <summary>
        /// Boot
        /// </summary>
        public static void Boot() => QrngSeeder.Services.Add(typeof(HuQrngSeeder));
    }
}
