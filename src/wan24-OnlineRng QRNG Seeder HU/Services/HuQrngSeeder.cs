﻿using System.Text;
using wan24.Core;
using wan24.Crypto;
using wan24.OnlineRng.Config;

namespace wan24.OnlineRng.Services
{
    /// <summary>
    /// HU QRNG seeder
    /// </summary>
    public sealed class HuQrngSeeder : TimedHostedServiceBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public HuQrngSeeder() : base(HuQrngSettings.Current.SeedInterval) => Name = "HU QRNG seeder";

        /// <summary>
        /// Receive and process online seed
        /// </summary>
        /// <param name="cancellationToken">Cancellation token</param>
        public Task SeedAsync(CancellationToken cancellationToken = default) => IfUndisposed(() => TimedWorkerAsync(cancellationToken));

        /// <inheritdoc/>
        protected override Task TimedWorkerAsync() => TimedWorkerAsync(CancelToken);

        /// <summary>
        /// Timed worker
        /// </summary>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>If seeded successfully</returns>
        private async Task<bool> TimedWorkerAsync(CancellationToken cancellationToken)
        {
            using SemaphoreSyncContext ssc = await WorkerSync.SyncContextAsync(cancellationToken).DynamicContext();
            Memory<byte> seed = default;
            try
            {
                seed = RequestSeed();
                if (seed.Length < 1) return false;
                using RentedArrayStructSimple<byte> buffer = new(seed.Length, clean: false)
                {
                    Clear = true
                };
                await RND.FillBytesAsync(buffer.Memory).DynamicContext();
                seed.Span.Xor(buffer.Span);
                await RND.AddSeedAsync(seed, cancellationToken).DynamicContext();
                return true;
            }
            catch (Exception ex)
            {
                if (ServiceTask is null || cancellationToken != CancelToken) throw;
                ErrorHandling.Handle(new($"{GetType()} failed to get and process seed", ex, Constants.CRYPTO_ERROR_SOURCE));
                return false;
            }
            finally
            {
                seed.Span.Clean();
            }
        }

        /// <summary>
        /// Request fresh seed online
        /// </summary>
        /// <returns>Received seed bytes</returns>
        private Memory<byte> RequestSeed()
        {
            int red = 0,
                res = 0;
            bool connected = false;
            byte[] seed = new byte[256];
            try
            {
                if (!new QRNG().CheckDLL())
                {
                    Logging.WriteError("Failed to load HU QRNG library");
                    return Array.Empty<byte>();
                }
                res = HuQrngSettings.Current.SSL
                    ? QRNG.qrng_connect_SSL(new StringBuilder(32).Insert(0, HuQrngSettings.Current.User), new StringBuilder(32).Insert(0, HuQrngSettings.Current.Password))
                    : QRNG.qrng_connect(new StringBuilder(32).Insert(0, HuQrngSettings.Current.User), new StringBuilder(32).Insert(0, HuQrngSettings.Current.Password));
                if (res != 0)
                {
                    Logging.WriteError($"Failed to connect to HU QRNG service (SSL {HuQrngSettings.Current.SSL}): {QRNG.qrng_error_strings[res]}");
                    return Array.Empty<byte>();
                }
                connected = true;
                res = QRNG.qrng_get_byte_array(ref seed[0], seed.Length, ref red);
                if (res != 0)
                {
                    Logging.WriteWarning($"Failed to receive random data from HU QRNG service (SSL {HuQrngSettings.Current.SSL}): {QRNG.qrng_error_strings[res]}");
                    return Array.Empty<byte>();
                }
            }
            finally
            {
                if (res != 0) seed.Clear();
#pragma warning disable CA1806 // Doesn't use return value
                if (connected) QRNG.qrng_disconnect();
#pragma warning restore CA1806 // Doesn't use return value
            }
            if (red == 0)
            {
                Logging.WriteWarning($"{GetType()} got no seed");
                return Array.Empty<byte>();
            }
            Logging.WriteDebug($"{GetType()} got {red} byte seed");
            if (red != seed.Length)
                Logging.WriteWarning($"{GetType()} expected {seed.Length} byte seed, but got only {red} byte instead");
            return seed.AsMemory(0, red);
        }

        /// <inheritdoc/>
        protected override async Task BeforeStartAsync(CancellationToken cancellationToken)
        {
            await base.BeforeStartAsync(cancellationToken).DynamicContext();
            if (!await TimedWorkerAsync(cancellationToken).DynamicContext())
                throw await CryptographicException.FromAsync($"Initial seeding of HU QRNG seed timer ({GetType()}) {GUID} (\"{Name}\") failed", new IOException());
        }
    }
}
